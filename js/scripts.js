$(document).ready(function() {
	 $('.tknc a').click(function(e){
    	e.preventDefault();
    	$('.tknc-mr').slideToggle();
    });
	
    $('.header-tab ul li').click(function() {
        $('.page-not-tab').css('display', 'none');
    })
    $('#example').dataTable({
        "lengthMenu": [
            [5, 15, 25, 50, -1],
            [5, 15, 25, 50, "All"]
        ],
        "dom": 't<"bottom-table"<"left"ip>l><"clear">',

        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Trang _START_ đến _END_ (_TOTAL_ bản ghi)",
            "sInfoEmpty": "Trang 0 đến 0 (0 bản ghi)",
            "sInfoFiltered": "(được lọc từ _MAX_ bản ghi)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "oPaginate": {
                "sNext": '<img src="images/next.svg" height="9px" width="5px"/>',
                "sPrevious": '<img src="images/pre.svg" height="9px" width="5px"/>',
                "sFirst": '<img src="images/pre_all.svg" height="9px" width="5px"/>',
                "sLast": '<img src="images/next-all.svg" height="9px" width="5px"/>'
            },
            "responsive": true
        }
    });
    $('#example1').dataTable({
        "lengthMenu": [
            [10, 15, 25, 50, -1],
            [10, 15, 25, 50, "All"]
        ],
        "dom": 't<"bottom-table"<"left"ip>l><"clear">',

        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Trang _START_ đến _END_ (_TOTAL_ bản ghi)",
            "sInfoEmpty": "Trang 0 đến 0 (0 bản ghi)",
            "sInfoFiltered": "(được lọc từ _MAX_ bản ghi)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "oPaginate": {
                "sNext": '<img src="images/next.svg" height="9px" width="5px"/>',
                "sPrevious": '<img src="images/pre.svg" height="9px" width="5px"/>',
                "sFirst": '<img src="images/pre_all.svg" height="9px" width="5px"/>',
                "sLast": '<img src="images/next-all.svg" height="9px" width="5px"/>'
            },
            "responsive": true
        }
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
    $('#example2,#example3,#example4').dataTable({
        "lengthMenu": [
            [5, 15, 25, 50, -1],
            [5, 15, 25, 50, "All"]
        ],
        "dom": 't<"bottom-table"<"left"ip>l><"clear">',

        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Trang _START_ đến _END_ (_TOTAL_ bản ghi)",
            "sInfoEmpty": "Trang 0 đến 0 (0 bản ghi)",
            "sInfoFiltered": "(được lọc từ _MAX_ bản ghi)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "oPaginate": {
                "sNext": '<img src="images/next.svg" height="9px" width="5px"/>',
                "sPrevious": '<img src="images/pre.svg" height="9px" width="5px"/>',
                "sFirst": '<img src="images/pre_all.svg" height="9px" width="5px"/>',
                "sLast": '<img src="images/next-all.svg" height="9px" width="5px"/>'
            },
            "responsive": true
        }
    });
    $('.bottom-table .left .pagination .previous a').html('<img src="images/pre.svg" height="9px" width="5px"/>');
    $('.bottom-table .left .pagination .next a').html('<img src="images/next.svg" height="9px" width="5px"/>');

    $('.form_date').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        pickerPosition: "bottom-left"
    });
    $('.date').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        pickerPosition: "bottom-right"
    });
    $('.example_input').dayMonthYearCalendar({
        container: $('.selects_container'),
        minDate: new Date(1900, 01, 01),
        hideInput: true	
    });
   
});